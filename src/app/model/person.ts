export class Person {
    constructor(public name: string, public height: number,
                public mass: number, public hair_color: string) {}
}
