import { Injectable } from '@angular/core';
import { Person } from '../model/person';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PeopleService {

  id: number;

  constructor(private httpClient: HttpClient) { }

  getPerson(): Observable<Person> {
    return this.httpClient.get<Person>(environment.backendUrl + '/people/' + this.id);
  }

  getId() {
    return this.id;
  }
  setId(id: number) {
    this.id = id;
  }
}
