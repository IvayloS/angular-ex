import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CallerService {

  name: string;
  id: number;

  constructor(private httpClient: HttpClient) { }

  getSWObject(): Observable<Object> {
    return this.httpClient.get<Object>(environment.backendUrl + '/' + this.name + '?page=' + this.id);
  }

  getId() {
    return this.id;
  }

  setId(id: number) {
    this.id = id;
  }

  getName() {
    return this.name;
  }

  setName(name: string) {
    this.name = name;
  }
}
