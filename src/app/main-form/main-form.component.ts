import { Component, OnInit } from '@angular/core';
import { CallerService } from '../services/caller.service';

@Component({
  selector: 'app-main-form',
  templateUrl: './main-form.component.html',
  styleUrls: ['./main-form.component.css']
})
export class MainFormComponent implements OnInit {

  SWObject: string;
  id: number;
  data: any;

  acceptableSWOBjects = ['people', 'starships', 'planets', 'films', 'species', 'vehicles'];
  acceptableIds = [1, 2, 3, 4, 5];

  tHeads = [];


  constructor(private callerService: CallerService) { }

  ngOnInit() {
  }

  searchSelection() {
    console.log(this.SWObject, this.id);

    if (!this.acceptableSWOBjects.includes(this.SWObject)
        || (this.id > 5 || this.id < 1)) {

      alert(`The input fields should contain a member of the
specified star wars elements and a number between 1 and 5 (inclusive)`)

    } else {

      this.callerService.setName(this.SWObject);
      this.callerService.setId(this.id);

      this.callerService.getSWObject().subscribe(
        data => {
          console.log(data);
          this.data = data;

          for (let item of data['results']) {

            for (let elm of Object.keys(item)) {
              this.tHeads.push(elm);
            }
          }
        }, error => {
          console.log(error);
          alert(error.error.detail + " | " + error.message);
        }
      );

    }
  }

}
